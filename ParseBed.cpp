#include "ParseBed.h"

ParseBed::ParseBed()
{
    
}


void ParseBed::parse(ParseBam *bam, const std::string &fileName)
{
    std::ifstream fh(fileName.c_str());

    // check if object is valid
    if (!fh)
    {
        std::cerr << "ParseBed::parse failed to open file to read " << fileName << std::endl;
        return;
    }

    // define histogram
    const int bucketCount = 120;
    const double bucketMin = -0.3;
    const double bucketMax = 1.3;
    const double bucketSpan = bucketMax - bucketMin;
    const double bucketSize = 1.0 / bucketCount;

    uint32_t counter = 0;
    while (fh.peek() != EOF)
    {
        BedLine line;
        fh >> line;

        // skip no CDS
        if (line.thickStart == line.thickEnd)
            continue;

        counter++;

        // parse exons
        line.parseExons();
        
        // get reads per transcript
        std::string queryRegion = line.chrom + ":" + std::to_string(line.chromStart) + "-" + std::to_string(line.chromEnd);
        std::vector<uint32_t> reads;
        bam->query(reads, queryRegion);
        uint32_t readOffset;

        // construct histogram
        std::vector<uint32_t> histogram(bucketCount + 1);


        uint32_t reads_used = 0;
        for (auto k = 0; k < reads.size(); k++)
        {
            if (line.toLinear(readOffset, reads[k]))
            {
                double readRelative = 0.0;
                if (line.strand == '+')
                {
                    readRelative = ((double)readOffset - line.cdsStart) / line.cdsSpan;
                }
                else
                {
                    readRelative = ((double)line.cdsEnd - readOffset) / line.cdsSpan;
                }

                if ((bucketMin <= readRelative) && (readRelative <= bucketMax))
                {
                    reads_used++;
                    int bucket = (int)std::floor(((readRelative - bucketMin) / bucketSpan) / bucketSize);
                    //std::cout << readRelative << '\t' << bucket << '\n';
                    histogram[bucket] += 1;
                }
            }
        }

        
        if (reads_used > 0)
        {
            std::cout << line.name << '\t' << reads_used << '\t';
            for (uint32_t value : histogram)
            {
                std::cout << value << ',';
            }
            std::cout << std::endl;
        }   
    }

    std::cerr << "counter " << counter << std::endl;
    

    fh.close();
}