#include "ParseBam.h"

ParseBam::ParseBam() :
    m_fhBam(nullptr),
    m_fhBai(nullptr),
    m_header(nullptr),
    m_bam(nullptr)
{

}

ParseBam::~ParseBam()
{
    if (m_bam != nullptr)
        bam_destroy1(m_bam);

    if (m_header != nullptr)    
        bam_hdr_destroy(m_header);
    
    if (m_fhBai != nullptr)
        hts_idx_destroy(m_fhBai);

    if (m_fhBam != nullptr)    
        sam_close(m_fhBam);
}


void ParseBam::open(const std::string &fileBam, const std::string &fileBai)
{
    // open bam file handle
    m_fhBam = hts_open(fileBam.c_str(), "r");
    if (m_fhBam == nullptr)
    {
        std::cerr << "ParseBam::parse failed to open BAM file " << fileBam << std::endl;
        return;
    }

    // read bam header
    m_header = sam_hdr_read(m_fhBam);
    if (m_header == nullptr)
    {
        std::cerr << "ParseBam::parse failed to read BAM header" << std::endl;
        return;
    }

    // open bam index
    m_fhBai = sam_index_load(m_fhBam, fileBai.c_str());
    if (m_fhBai == nullptr)
    {
        std::cerr << "ParseBam::parse failed to read BAM index " << fileBai << std::endl;
        return;
    }

    // initialise bam record
    m_bam = bam_init1();
}


void ParseBam::query(std::vector<uint32_t> &reads, const std::string &queryRegion)
{
    hts_itr_t *iter;

    if ((iter = bam_itr_querys(m_fhBai, m_header, queryRegion.c_str())) == 0) 
    {
        std::cerr << "ParseBam::query failed to find " << queryRegion << std::endl;
	} 
    int r = 0;
    
    while ((r = bam_itr_next(m_fhBam, iter, m_bam)) >= 0)
    {
        uint32_t readStart = m_bam->core.pos + 1;
        reads.push_back(readStart);
    }
    hts_itr_destroy(iter);
}