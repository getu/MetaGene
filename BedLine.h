#ifndef BEDLINE_H
#define BEDLINE_H

#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>

#include "IntervalTree.h"

class BedLine
{
public:
    BedLine();

    uint8_t strand;
    uint32_t chromStart;
    uint32_t chromEnd;
    uint32_t thickStart;
    uint32_t thickEnd;
    uint32_t score;
    uint32_t blocks;
    uint32_t cdsStart;
    uint32_t cdsEnd;
    uint32_t cdsSpan;
    uint32_t span;
    std::string chrom;
    std::string name;
    std::string itemRgb;
    std::vector<uint32_t> blockSizes;
    std::vector<uint32_t> blockStarts;

    void swap(BedLine &other);
    static void listToArray(std::vector<uint32_t> &array, const std::string &list);
    friend std::istream& operator>> (std::istream& in, BedLine &data);
    void parseExons();
    bool toLinear(uint32_t &readOffset, uint32_t readStart);

private:
    struct ExonNode
    {
        uint32_t exonStart;
        uint32_t exonEnd;
        uint32_t offset;
    };

    //IntervalTree<uint32_t, ExonNode> m_exonTree;
    
    struct ExonCompare
    {
        // overlapping ranges are considered equivalent
        bool operator()(const ExonNode& lhv, const ExonNode& rhv) const
        {
            return lhv.exonStart < rhv.exonStart && lhv.exonEnd < rhv.exonStart;
        }
    };

    std::set<ExonNode, ExonCompare> m_exonTree;
    
};

#endif /* BEDLINE_H */