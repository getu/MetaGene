#ifndef PARSEBED_H
#define PARSEBED_H

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>

#include "BedLine.h"
#include "ParseBam.h"

class ParseBed
{
public:
    ParseBed();
    void parse(ParseBam *bam, const std::string &fileName);

private:

};

#endif /* PARSEBED_H */