#ifndef PARSEBAM_H
#define PARSEBAM_H

#include <iostream>
#include <string>
#include <vector>
#include <htslib/sam.h>
#include <htslib/kstring.h>
#include <htslib/kseq.h>
#include <zlib.h>

class ParseBam
{
public:
    ParseBam();
    ~ParseBam();

    void open(const std::string &fileBam, const std::string &fileBai);
    void query(std::vector<uint32_t> &reads, const std::string &queryRegion);

    samFile *m_fhBam;
    hts_idx_t *m_fhBai;
    bam_hdr_t *m_header;
    bam1_t *m_bam;
};

#endif