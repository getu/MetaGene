#include <iostream>

#include "ParseBed.h"
#include "ParseBam.h"
#include "IntervalTree.h"

struct ExonProps
{
    uint32_t exonStart;
    uint32_t offset;
};


int main(int argc, const char *argv[])
{
    std::string fileBed = "/Users/tushevg/Desktop/RiboData/bed/ncbiRefSeq_rn6_Sep2018.bed";
    std::string fileBam = "/Users/tushevg/Desktop/RiboData/bams/MonoVsPoly/Total_01_genome_umi.bam";
    std::string fileBai = "/Users/tushevg/Desktop/RiboData/bams/MonoVsPoly/Total_01_genome_umi.bam.bai";

    // parse BAM file
    ParseBam *bam = new ParseBam();
    bam->open(fileBam, fileBai);

    // parse BED file
    ParseBed *bed = new ParseBed();
    bed->parse(bam, fileBed);
 
    return 0;
}